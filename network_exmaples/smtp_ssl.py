#!/usr/bin/env python3

import smtplib, ssl

srv = "mail.4tester.net"
port = 465  # For starttls
sender = "regev@4tester.net"
receiver = "alex@vaiolabs.com"
passwd = "Cyber123456!"
msg="""
this is test

"""
# Create a secure SSL context
context = ssl.create_default_context()

with smtplib.SMTP_SSL(srv, port) as smtp_server:
    # Login with your Gmail account using SMTP
    smtp_server.login(sender,passwd)

    # We'll be sending this message in the above format (Subject:...\n\nBody)
    smtp_server.sendmail(sender, receiver, msg)