#!/usr/bin/env python3
#################################################################
#created by : silent-mobius
#purpose: automate ssh server install and config
#date: 10/02/2021
#version: v1.0.0
##################################################################
import os
import sys
import platform
import time
from os import chmod
from os import system

try:
    import paramiko
except :
    print("library not found")
    print("trying to install")
    system("pip3 install --user paramiko")
    import paramiko


    #######
    #general functions
    ########

def deco(str_arr):
    os.system('clear')
    print('#'*30)
    print(f'# {str_arr}')
    print('#'*30)
    time.sleep(3)
    os.system('clear')

def os_type():
    '''
    function that will tell you on which nix distro you are working on
    '''
    _system = platform.system()

    if (_system != 'Linux') or (_system != 'linux'):
        system('clear')
        deco('System Not Supported')        
        sys.exit(1)
    else:
        try:
            import distro
        except:
            print('there was a error\n let me try to fix it\n')
            system('pip3 install --user distro')
            import distro

        return distro.id()


def ssh_server_install(_os_type='fedora'):
    if _os_type == 'fedora' or _os_type == 'centos' or _os_type == 'redhat':
        system("yum -y install openssh-server")
    elif _os_type == 'debian' or _os_type == 'ubuntu' or _os_type == 'linuxmint' :
        system('apt-get install -y openssh-server')
    else:
        deco("system not supported")
        sys.exit(1)